#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <pwd.h>
#include "check_pass.h"


int main(int argc, char *argv[])
{
    struct stat sb;
    int f, count;
    char md[20];//demander un mot de passe

    if (argc < 1) {
        return printf("Erreur arguments");
        exit(1);
    }
    
    // le premier argument est le fichier qu'on veut effacer
    //on recupere ses informations
    stat(argv[1], &sb);
    gid_t gid = getgid();
    // on verifie que l'utilisateur qui à lancé le programme appartient au meme groupe que le fichier
    if (sb.st_gid != gid ){
        printf("erreur de permission");
        exit(1);
    }

    printf("Veuillez entrez un mot de passe: ");
    scanf("%s", md);

    uid_t ruid = getuid();

    // pour stocker le mot de passe on cree une structure
    struct passwd *p = getpwuid(ruid);// struct passwd *getpwuid(uid_t uid); dans la documentation
    // elle retourne un pointeur vers une structure passwd avec une structure defini dans <pwd.h>
    // char    *pw_name   User's login name. [exemple]

   if (check_pass(p->pw_name, md) == 1){
        if (remove(argv[1])==0){
            printf("Suppression");
        }
    }
    else{
        printf("Mot de passe incorrect");
        exit(EXIT_FAILURE);
    }

    return 0;

}

