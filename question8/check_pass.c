#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int check_pass(char* utilisateur, char* mdp) {

    FILE * f;
    char * ligne = NULL;
    size_t n = 0;
    ssize_t read;

    f = fopen("home/admin/passwd", "r");
    if (f == NULL){
        printf("can't access file");
        exit(EXIT_FAILURE);
    }

    //lire le fichier
    while ((read = getline(&ligne, &n, f)) != -1) {

        n = strlen(ligne);
        if( ligne[n-1] == '\n' )
            ligne[n-1] = 0;

        char * token = strtok(ligne, ":");//extraire un a un les chaine separe par :

        if (strcmp(token, utilisateur)==0){
            token = strtok(NULL, " ");
            if (strcmp(token, mdp)==0){
                return 1;
            }
            else{
                return 0;
            }
        }
    }

    fclose(f);
    return 0;
}
