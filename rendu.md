# Rendu "Les droits d’accès dans les systèmes UNIX"

## Binome

- SAOUDI, SARA, email: sara.saoudi.etu@univ-lille.fr

- KESRAOUI, NASSIMA, email:nassima.kesraoui.etu@univ-lille.fr

## Question 1

Ajouter un utilisateur:

```
sudo adduser toto
```
Ajouter l'utilisateur toto au groupe ubuntu:

```
sudo adduser toto ubuntu
```

toto n'a pas le droit d'ecrire dans le fichier titi, tout d'abord le processus vérifie les droits d'accès de titi, il voit que son EUID(l'utilisateur) ==proprietaire de titi, alors il regarde que le premier triplet r-- et donc le processus n'a pas le droit d'ecrire dans titi 


## Question 2

Le caractére x pour un répertoire signifie aussi l'executer ou/et y accéder.

- quand on essaye d'acceder a mydir avec l'utilisateur toto ça affiche : 
```
bash: cd: mydir: Permission denied
```
car comme toto appartient au groupe ubuntu mais que on a enelvé 
les droits de mydir pour le groupe ubuntu donc il ne peut pas y acceder.

-Quand on essaye d'acceder a data.txt a partir de toto
```
ls: cannot access 'mydir/.': Permission denied
ls: cannot access 'mydir/..': Permission denied
ls: cannot access 'mydir/data.txt': Permission denied
total 0
d????????? ? ? ? ?            ? .
d????????? ? ? ? ?            ? ..
d????????? ? ? ? ?            ? data.txt

```
explication: avec toto on peut voir le contenu par exemple le nom des fichiers mais pas les droits et autres informations ni y acceder


## Question 3

en executant suid.c avec toto ça renvoie:
```
1001
1001
1001
ficher ne peut pas être trouvé
```

apres qu'on ait ativer le flag, l'exécution de suid par toto donne:

```
1001
1000
1001
hola
```

on peut exécuter le programme et acceder au fichier mydata.txt et le lire
apres avoir activer le flag on remarque que le EUID change et passe à 1000 celui de ubuntu alors que celui de toto est 1001


Apres changement grace à set-user-id, le EUID change passant de 1001 à 1000 de plus nous pouvons acceder au contenu du fichier stocké dans
mydir en lancant notre programme.


## Question 4
le programme retourne ceci apres son execution dans toto:

```
la valeur de EUID = 1001
la valeur de EGID = 1001
fichier introuvable
```
apres avoir activier le flag on a toujours le fichier introuvable
il faut pas faire chmod u+s suid.py mais sudo chmod u+s /usr/bin/python3.8


```
la valeur de EUID = 0
la valeur de EGID = 1001
Le fichier s'est bien ouvert 

```
## Question 5

la commande chfn :permet de modifier les informations personnel des utilisateurs contenu dans le fichier "/etc/passwd".

```
-rwsr-xr-x 1 root root 85064 May 28  2020 /usr/bin/chfn
```
rws :le premier triplet signifie que le programme est autorisé à être exécuté avec l'id effectif du propriétaire (ubuntu par exemple) lorsqu'il est exécuté par n'importe quel utilisateur (toto par exemple)

avec l'utilisateur toto en lancant la commande /etc/passwd on a :

```
     ...
toto:x:1001:1001::/home/toto:/bin/bash
```
puis avec chfn on a :
```
Password: 
Changing the user information for toto
Enter the new value, or press ENTER for the default
	Full Name: 
	Room Number []: toto
	Work Phone []: 0789564757
	Home Phone []: 5684848946

```
on relance la commande /etc/passwd:

```
toto:x:1001:1001:,toto,0789564757,5684848946:/home/toto:/bin/bash

```
## Question 6

Les mots de passe dans les système Unix/Linux sont stockés dans le fichier ‘/etc/shadow’.
ces mots de passe sont stockés dans le fichier ‘/etc/shadow’ ne peuvent être lu que par le root.
Vous verrez une liste d’utilisateur suivis d’un hash. Ce hash contient les informations de votre mot de passe.

## Question 7

Mettre les scripts bash dans le repertoire *question7*.

## Question 8

Le programme et les scripts dans le repertoire *question8*.

## Question 9

Le programme et les scripts dans le repertoire *question9*.

## Question 10

Les programmes *groupe_server* et *groupe_client* dans le repertoire
*question10* ainsi que les tests. 








