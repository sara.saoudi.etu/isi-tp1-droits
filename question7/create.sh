#!/bin/bash


#creation des groupes

sudo groupadd groupe_a
sudo groupadd groupe_b
sudo groupadd groupe_c

#creation de l'admin

sudo groupadd admin
sudo adduser admin --ingroup admin

#utilisateur qui appartient a un group
sudo adduser lambda_a --ingroup groupe_a 

sudo adduser lambda_b --ingroup groupe_b


#creation des répertoires
mkdir dir_a
mkdir dir_b
mkdir dir_c


#lambda_a peut lire tous les fichiers de dir_a et dir_c => r
#lambda_a ne peut pas modifier les fichiers de dir_c, ni les renommer ni effacer ni creer
#lambda_a n'ont aucun droit sur dir_b

#pour changer le groupe des dossiers

sudo chown admin:groupe_a dir_a
#on doit donner les droits de lecture et d'ecriture 
sudo chmod g+rwx dir_a
#les autres utilisateurs n'ont aucun droits sur dir_a et ses fichiers
sudo chmod o-rwx dir_a dir_b

sudo chmod g+s dir_a dir_b #definit l'id de groupe (setgid) sur le répertoire en cours, écrit en tant que .., tous les nvx fichiers et ss-repertoires dans le repertoire en cours héritent de l'id de groupe du repertoire plutot que l'id de groupe principal de l'utilisateur qui a cree le fichier

sudo chmod +t dir_a dir_b dir_c # un fichier qui se trouve dans l'arborescene qui a comme racine l'un des repertoires, ne peut être renommé ou effacé que par le proprietaire du répertoire ou par le proprietaire du fichier

sudo chown admin:groupe_b dir_b
sudo chmod g+rwx dir_b



sudo chown admin: dir_c
sudo chmod u+rwx dir_c
sudo chmod o-w dir_c



